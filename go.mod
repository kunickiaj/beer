module github.com/kunickiaj/beer

require (
	github.com/BurntSushi/toml v0.3.1 // indirect
	github.com/andygrunwald/go-jira v1.6.0
	github.com/fatih/structs v1.1.0 // indirect
	github.com/go-git/go-git/v5 v5.1.0
	github.com/google/go-querystring v1.0.0 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/mitchellh/go-homedir v1.1.0
	github.com/pkg/errors v0.8.1
	github.com/sirupsen/logrus v1.3.0
	github.com/spf13/afero v1.2.0 // indirect
	github.com/spf13/cobra v0.0.3
	github.com/spf13/viper v1.3.1
	github.com/trivago/tgo v1.0.5 // indirect
)

go 1.13
